from api.dto.PlotData import PlotData
from massiveOES import Spectrum


def spectrum_to_plot_data(spectrum: Spectrum) -> PlotData:
    return PlotData(x=spectrum.x.tolist(), y=spectrum.y.tolist())
