import os

from massiveOES import MeasuredSpectra


class IOManager:

    @staticmethod
    def load_measured_spectra_from_file(file_path):
        filename, extension = os.path.splitext(file_path)
        if (extension == '.csv') or (extension == '.CSV'):
            return MeasuredSpectra.from_CSV(file_path)
        elif extension == '.txt' or (extension == '.TXT'):
            return MeasuredSpectra.from_FHRfile(file_path, [])
        elif (extension == '.oes') or (extension == '.OES'):
            return MeasuredSpectra.load(file_path)
        elif (extension == '.json') or (extension == '.JSON'):
            return MeasuredSpectra.from_json(file_path)