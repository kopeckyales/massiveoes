from bisect import bisect_left
from typing import List


class LinearizationManager:

    @staticmethod
    def value_to_closest_index(value: float, sequence: List[float]):
        position = bisect_left(sequence, value)
        if position == len(sequence):
            return position-1
        return position
