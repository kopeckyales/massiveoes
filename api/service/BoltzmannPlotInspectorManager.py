import numpy
import pandas

from api.dto.VibLevel import VibLevel
from massiveOES import spectrum


class BoltzmannPlotInspectorManager:

    @staticmethod
    def prep_db(database):
        # for key in database.keys():
        database['E'] = database['E_J'] + database['E_v']
        database['degeneracy'] = 2 * database['J'] + 1
        # database[key]['specie'] = key

        return database

    @staticmethod
    def prep_table(database_full):
        species_bp = []
        for key in database_full['specie'].unique():
            vibration_levels = database_full['v'].unique()
            for level in vibration_levels:
                specie_bp = VibLevel(key, level, True)
                species_bp.append(specie_bp)
        return species_bp

    @staticmethod
    def prep_data_frame(species_bp, database_full):
        partial_frames = []
        for specie_vib in species_bp:
            if specie_vib.show_vib:
                partial_frames.append(database_full.loc[(database_full.v == specie_vib.vibration_level) & (database_full.specie == specie_vib.specie_bp)])

        if partial_frames:
            return pandas.concat(partial_frames)
        else:
            return None

    @staticmethod
    def find_state(database_for_plot, data):
        select_frame = pandas.DataFrame(columns=database_for_plot.columns)
        for coords in data:
            row = database_for_plot.loc[(database_for_plot.E == coords[0]) & ((database_for_plot.pops/database_for_plot.degeneracy) == coords[1])]
            if row.empty:
                print('Data not found WTF!!!!')
                return
            select_frame = select_frame.append(row)
        return select_frame

    @staticmethod
    def provide_line_chart_points(specs, database_full, current_spectra):
        y = numpy.dot(specs[:, database_full.index], database_full.pops)
        simulated = spectrum.Spectrum(x=current_spectra, y=y)
        return simulated

    @staticmethod
    def provide_selected_points_info_list(data_frame: pandas.DataFrame):
        info_list = []
        for index, specie in enumerate(data_frame['specie'].tolist()):
            info_list.append({
                'specie': specie,
                'v': data_frame['v'].tolist()[index],
                'J': data_frame['J'].tolist()[index],
                'E': data_frame['E'].tolist()[index],
                'pops': data_frame['pops'].tolist()[index]
            })
        return info_list
