from api.service.IOManager import IOManager


class MeasuredSpectraManager:

    @staticmethod
    def load_measurement_from_file(path):
        return IOManager.load_measured_spectra_from_file(path)
