from typing import Dict

from pandas import DataFrame

from api.dto.bp_inspector.BPInspectorData import BPInspectorData
from api.dto.linearize.LinearizationData import LinearizationData
from massiveOES import MeasuredSpectra, SpecDB, Parameters


def inject_context(func):
    def func_wrapper(sid, *args):
        context = app_context_store[sid]
        return func(context, *args)
    return func_wrapper


class AppContext:
    def __init__(self):
        self.measured_spectra: MeasuredSpectra = None
        # self.__simulations: SimulationDictionary = {}
        self.active_measurement_set: str = None
        self.bp_inspector_data: BPInspectorData = BPInspectorData()
        self.bp_database_for_plot: DataFrame = DataFrame()
        self.bp_database_selection: DataFrame = DataFrame()
        self.bp_specs = None
        self.__linearize_data: LinearizationDataDictionary = {}

    @property
    def current_linearize_data(self) -> LinearizationData:
        if self.active_measurement_set not in self.__linearize_data:
            self.__linearize_data[self.active_measurement_set] = LinearizationData.from_app_context(self)
        return self.__linearize_data[self.active_measurement_set]

    @property
    def current_parameters(self) -> Parameters:
        return self.measured_spectra.parameters_list[self.active_measurement_set]

    @property
    def current_simulations(self) -> Dict[str, SpecDB]:
        ret = {}
        for simulation_name in self.current_parameters.info['species']:
            ret[simulation_name] = self.measured_spectra.simulations[simulation_name]
        return ret

    @property
    def current_measured_spectra_for_fit(self):
        return self.measured_spectra.measured_Spectra_for_fit(self.active_measurement_set)

    @property
    def current_measured_spectra_for_linearize(self):
        params = {}
        for param in self.current_linearize_data.parameters:
            params[param.parameter_name] = param.to_lm_fit_parameter()
        return self.measured_spectra.measured_Spectra_for_fit(self.active_measurement_set, params)

    def destroy_current_linearize_data(self):
        if self.active_measurement_set in self.__linearize_data:
            del self.__linearize_data[self.active_measurement_set]

    @staticmethod
    def get(sid: str):
        return app_context_store[sid]

    @staticmethod
    def create(sid: str):
        app_context_store[sid] = AppContext()

    @staticmethod
    def destroy(sid: str):
        del app_context_store[sid]


LinearizationDataDictionary = Dict[str, LinearizationData]

ContextStore = Dict[str, AppContext]
SimulationDictionary = Dict[str, Dict[str, SpecDB]]
app_context_store: ContextStore = {}
