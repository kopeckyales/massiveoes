import glob

import os
from typing import List, Dict

import copy

from api import AppContext
from api.dto.FitParameters import FitParameters
from api.dto.LmFITParameter import LmFITParameter
from api.dto.WorkspaceData import WorkspaceData
from api.exception.AsyncEmitNecessaryException import AsyncEmitNecessaryException
from api.service.MeasuredSpectraManager import MeasuredSpectraManager
from massiveOES import SpecDB

LmFITParameterList = List[LmFITParameter]


def open_measurement(app_context: AppContext, data):
    app_context.measured_spectra = MeasuredSpectraManager.load_measurement_from_file(data)
    app_context.active_measurement_set = 0
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def change_set(app_context: AppContext, set_id):
    app_context.active_measurement_set = set_id
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def update_parameters(app_context: AppContext, data: LmFITParameterList):
    for view_parameter in data:
        parameter = app_context.current_parameters.prms[view_parameter.parameter_name]
        view_parameter.to_lm_fit_parameter(parameter)
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def list_simulations():
    root_dir_path = os.path.join(os.path.dirname(__file__), '..', '..', 'massiveOES')
    sims = glob.glob(os.path.join(root_dir_path, 'data', '*db'))
    sims = [os.path.split(f)[-1] for f in sims]
    return sims


def add_simulation(app_context: AppContext, simulation):
    measurement_set = app_context.active_measurement_set
    spectra = app_context.measured_spectra
    spec = SpecDB(simulation)
    spectra.add_specie(spec, measurement_set)
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def fit(app_context: AppContext, data: FitParameters):
    app_context.measured_spectra.fit(app_context.active_measurement_set,
                                     method=data.active_fit_method,
                                     by_peaks=False,
                                     show_it=False,
                                     only_strong=True,
                                     weighted=False,
                                     maxiter=data.max_iterations
                                     )
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def copy_parameters(app_context: AppContext, data: Dict):
    __copy_parameters(app_context, data['source'], data['target'])
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def __copy_parameters(app_context: AppContext, source, target):
    app_context.measured_spectra.parameters_list[target] = \
        copy.deepcopy(app_context.measured_spectra.parameters_list[source])


def batch_fit(app_context: AppContext, data: Dict):
    for set_id in sorted(data['selected_sets']):
        if data['mode'] == 1:
            source_set_id = max([0, set_id - 1])
        elif data['mode'] == 2:
            source_set_id = data['values_from_constant']
        else:
            raise KeyError('Unsupported mode')
        app_context.active_measurement_set = set_id
        __copy_parameters(app_context, source_set_id, set_id)
        try:
            fit(app_context, data['fit_parameters'])
        except AsyncEmitNecessaryException as e:
            yield {
                'name': e.emit_name,
                'data': e.data
            }


def save_project(app_context: AppContext, file_path: str):
    filename, extension = os.path.splitext(file_path)
    # TODO: file exists exception
    if (extension == '.oes') or (extension == '.OES'):
        app_context.measured_spectra.save(file_path)
    elif (extension == '.json') or (extension == '.json'):
        app_context.measured_spectra.to_json(file_path)


def export_project(app_context: AppContext, file_path: str):
    # TODO: file exists exception
    app_context.measured_spectra.export_results(file_path)
