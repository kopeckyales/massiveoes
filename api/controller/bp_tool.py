from typing import Dict

import numpy
from scipy.constants import physical_constants
from scipy.stats import linregress

from api import AppContext
from api.dto.PlotData import PlotData
from api.dto.bp_inspector.ScatterDataSet import ScatterDataSet
from api.exception.AsyncEmitNecessaryException import AsyncEmitNecessaryException
from api.service.BoltzmannPlotInspectorManager import BoltzmannPlotInspectorManager


def fit(app_context: AppContext, data: Dict[str, Dict[str, str]]):
    bp_inspector_data = app_context.bp_inspector_data

    species = list(data.keys())

    db, specs = app_context.measured_spectra.fit_nnls(app_context.active_measurement_set, species, **data)
    database_full = BoltzmannPlotInspectorManager.prep_db(db)
    species_bp = BoltzmannPlotInspectorManager.prep_table(database_full)

    simulated = BoltzmannPlotInspectorManager.provide_line_chart_points(specs, database_full,
                                                                        app_context.current_measured_spectra_for_fit)

    bp_inspector_data.set_line_simulation_data(list(simulated.x.x), list(simulated.y))

    database_for_plot = BoltzmannPlotInspectorManager.prep_data_frame(species_bp, database_full)

    app_context.bp_database_for_plot = database_for_plot
    app_context.bp_specs = specs

    callsign = {}
    for key in database_for_plot['specie'].unique():
        vibration_levels = database_for_plot['v'].unique()
        for level in vibration_levels:
            callsign[key + ' ' + str(level)] = [key, level]

    for key, cs in callsign.items():
        selection = (database_for_plot.v == int(cs[1])) & (database_for_plot.specie == cs[0])
        data = PlotData()
        data.x = list(database_for_plot.loc[selection]['E'])
        data.y = list(database_for_plot.loc[selection]['pops'] / database_for_plot.loc[selection]['degeneracy'])
        # needs rework, quick solution
        indexes = [i for i, x in enumerate(data.y) if x == 0.0]
        data.x = [v for i, v in enumerate(data.x) if i not in indexes]
        data.y = [v for i, v in enumerate(data.y) if i not in indexes]
        callsign[key] = ScatterDataSet(key, data, cs[0], int(cs[1]))

    bp_inspector_data.set_simulation_parts(list(callsign.values()))
    raise AsyncEmitNecessaryException('bp_tool__update', app_context.bp_inspector_data)


def points_selected(app_context: AppContext, data):
    bp_inspector_data = app_context.bp_inspector_data
    if len(data['x']) > 0:
        zipped = zip(data['x'], data['y'])
        selection_df = BoltzmannPlotInspectorManager.find_state(app_context.bp_database_for_plot, zipped)
        app_context.bp_database_selection = selection_df

        simulated = BoltzmannPlotInspectorManager.provide_line_chart_points(
            app_context.bp_specs, selection_df, app_context.current_measured_spectra_for_fit)

        bp_inspector_data.set_line_simulation_data(list(simulated.x.x), list(simulated.y))
        bp_inspector_data.selected_points_table = BoltzmannPlotInspectorManager.provide_selected_points_info_list(
            selection_df)
    else:
        bp_inspector_data.set_line_simulation_data([], [])
        bp_inspector_data.selected_points_table = []
    raise AsyncEmitNecessaryException('bp_tool__update', app_context.bp_inspector_data)


kB = physical_constants['Boltzmann constant in inverse meters per kelvin'][0]/100


def calc_t_rot(app_context: AppContext):
    bp_database = app_context.bp_database_selection

    y = numpy.log(bp_database.pops / (numpy.array(bp_database.degeneracy, dtype=float)))

    slope, intercept, r_value, p_value, std_err = linregress(bp_database.E, y)
    t_rot = -1 / (kB * slope)
    t_rot_dev = abs(std_err / slope) * t_rot
    line_data = PlotData(x=list(bp_database.E), y=list(numpy.exp(bp_database.E * slope + intercept)))

    raise AsyncEmitNecessaryException('bp_tool__calculated_temperature', {
        'type': 'T_rot',
        'line_data': line_data,
        'value': float(t_rot),
        'deviation': float(t_rot_dev)
    })


def calc_t_vib(app_context: AppContext):
    bp_database = app_context.bp_database_selection
    x = bp_database.E_v.unique()
    y = []
    for ev in x:
        y.append(numpy.log(bp_database.pops[bp_database.E_v == ev].sum()))
    slope, intercept, r_value, p_value, std_err = linregress(x, y)
    t_vib = -1 / (kB * slope)
    t_vib_dev = abs(std_err / slope) * t_vib
    raise AsyncEmitNecessaryException('bp_tool__calculated_temperature', {
        'type': 'T_vib',
        # no regression line here?
        'line_data': {'x': list(), 'y': list()},
        'value': float(t_vib),
        'deviation': float(t_vib_dev)
    })


def export_to_csv(app_context: AppContext, path: str):
    app_context.bp_database_selection.to_csv(path)
