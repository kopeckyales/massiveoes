import os
from pathlib import Path, PurePath
from sys import platform


def list_files(parent_dir: str):
    if parent_dir == '/' and platform == 'win32':
        return win_platform_root()

    data_dir_content = [x for x in Path(parent_dir).iterdir()]
    return response(
        parent_dir,
        [str(PurePath(x)).replace('\\', '/')+"/" for x in data_dir_content if x.is_dir()],
        [str(PurePath(x)).replace('\\', '/') for x in data_dir_content
         if not x.is_dir() and x.suffix in ['.txt', '.csv', '.json', '.oes']]
    )


def win_platform_root():
    folders = [chr(x) + ":/" for x in range(65, 90) if os.path.exists(chr(x) + ":")]
    return response('/', folders, [])


def response(current: str, folders: list, files: list):
    a = 1
    return {
        'folders': folders,
        'files': files,
        'current': current
    }
