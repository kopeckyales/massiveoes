import numpy

from api.AppContext import AppContext
from api.dto.WorkspaceData import WorkspaceData
from api.dto.linearize.LinearizePair import LinearizePairList
from api.exception.AsyncEmitNecessaryException import AsyncEmitNecessaryException
from api.service.LinearizationMangaer import LinearizationManager


def default(app_context: AppContext):
    return app_context.current_linearize_data


def run_linearize(app_context: AppContext, data: LinearizePairList):
    x = list(map(lambda d: LinearizationManager.value_to_closest_index(
        d['measurement'], app_context.current_linearize_data.measurement_data.x), data))
    y = list(map(lambda d: d['simulation'], data))
    quadratic, linear, start = numpy.polyfit(x, y, 2)
    app_context.current_linearize_data.linearize_pairs = data
    for parameter in app_context.current_linearize_data.parameters:
        if parameter.parameter_name == 'wav_2nd':
            parameter.value = float(quadratic)
        elif parameter.parameter_name == 'wav_step':
            parameter.value = float(linear)
        elif parameter.parameter_name == 'wav_start':
            parameter.value = float(start)
    app_context.current_linearize_data.recompute_plot_data(app_context)
    return app_context.current_linearize_data


def save(app_context: AppContext):
    params = app_context.current_parameters
    for parameter in app_context.current_linearize_data.parameters:
        params.prms[parameter.parameter_name].value = parameter.value
    raise AsyncEmitNecessaryException('workspace__update', WorkspaceData.from_app_context(app_context))


def rollback(app_context: AppContext):
    app_context.destroy_current_linearize_data()
