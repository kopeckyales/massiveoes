class AsyncEmitNecessaryException(Exception):
    def __init__(self, emit_name: str, data):
        self.emit_name = emit_name
        self.data = data
