import copy

from api.dto.PlotData import PlotData
from api import AppContext
from api.dto.LmFITParameter import LmFITParameter, LmFITParameterList

from api.dto.linearize.LinearizePair import LinearizePairList
from massiveOES import puke_spectrum

from api.service import spectra_manager


class LinearizationData:
    def __init__(self):
        self.measurement_data = PlotData()
        self.simulation_data = PlotData()
        self.parameters: LmFITParameterList = list()
        self.linearize_pairs: LinearizePairList = list()

    @staticmethod
    def from_app_context(app_context: AppContext):
        self = LinearizationData()

        self.measurement_data = spectra_manager.spectrum_to_plot_data(
            app_context.current_measured_spectra_for_fit)

        self.simulation_data = spectra_manager.spectrum_to_plot_data(
            puke_spectrum(app_context.current_parameters, sims=app_context.current_simulations))

        # take only relevant parameters (first 3)
        parameters = [p for p in app_context.current_parameters.prms.values()][:3]
        self.parameters = list(map(lambda x: LmFITParameter.from_lm_fit_parameter(x), parameters))
        return self

    def recompute_plot_data(self, app_context:AppContext):
        self.measurement_data = spectra_manager.spectrum_to_plot_data(
            app_context.current_measured_spectra_for_linearize)

        linearize_parameters = copy.deepcopy(app_context.current_parameters)
        for parameter in self.parameters:
            linearize_parameters.prms[parameter.parameter_name].value = parameter.value
        self.simulation_data = spectra_manager.spectrum_to_plot_data(
            puke_spectrum(linearize_parameters, sims=app_context.current_simulations))