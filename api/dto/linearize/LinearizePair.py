from typing import List


class LinearizePair:
    """ Represents linearization measurement, simulation x-values pair"""
    def __init__(self):
        self.measurement: float = None
        self.simulation: float = None


LinearizePairList = List[LinearizePair]
