from api.dto.PlotData import PlotData


class MainPlotDataSet:
    def __init__(self):
        self.measurement_data = PlotData()
        self.simulation_data = PlotData()
        self.residual_data = PlotData()
