from typing import List

StrList = List[str]


class FitParameters:
    def __init__(self):
        self.max_iterations: int = 2000
        self.fit_methods_list: StrList = ['leastsq', 'nelder', 'lbfgsb']
        self.active_fit_method: str = self.fit_methods_list[0]
