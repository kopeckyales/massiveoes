from typing import List

from lmfit import Parameter


class LmFITParameter:
    def __init__(self):
        self.parameter_name: str = ''
        self.value = None
        self.min_value = None
        self.max_value = None
        self.use_for_fit: bool = False

    @staticmethod
    def from_lm_fit_parameter(parameter: Parameter):
        self = LmFITParameter()
        self.parameter_name = str(parameter.name)
        self.value = str(parameter.value)
        self.min_value = str(parameter.min)
        self.max_value = str(parameter.max)
        self.use_for_fit = parameter.vary
        return self

    def to_lm_fit_parameter(self, parameter: Parameter = None):
        if parameter is None:
            parameter = Parameter()
        parameter.value = float(self.value)
        parameter.min = float(self.min_value)
        parameter.max = float(self.max_value)
        parameter.vary = self.use_for_fit
        return parameter

    @property
    def float_min_value(self):
        return float(self.min_value)


LmFITParameterList = List[LmFITParameter]
