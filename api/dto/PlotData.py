class PlotData:
    def __init__(self, **kwargs):
        pair = kwargs.get('values', {'x': [], 'y': []})
        self.x = kwargs.get('x', pair['x'])
        self.y = kwargs.get('y', pair['y'])
