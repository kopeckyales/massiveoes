import string
from typing import List

from api import AppContext
from api.dto.FitParameters import FitParameters
from api.dto.LmFITParameter import LmFITParameter
from api.dto.MainPlotDataSet import MainPlotDataSet
from api.dto.PlotData import PlotData
from massiveOES import Spectrum, puke_spectrum
from massiveOES.spectrum import match_spectra, compare_spectra

LmFITParameterList = List[LmFITParameter]


class WorkspaceData:
    def __init__(self):
        self.active_measurement_set: string = None
        self.main_plot_data: MainPlotDataSet = MainPlotDataSet()
        self.parameters: LmFITParameterList = list()
        self.measurement_set_list = list()
        self.simulations = list()
        self.fit_parameters: FitParameters = FitParameters()

    @staticmethod
    def from_app_context(app_context: AppContext):
        self = WorkspaceData()
        self.active_measurement_set = app_context.active_measurement_set
        self.parameters = list(map(lambda x: LmFITParameter.from_lm_fit_parameter(x),
                                   [p for p in app_context.current_parameters.prms.values()]))
        self.measurement_set_list = list(range(len(app_context.measured_spectra.parameters_list)))
        self.main_plot_data.measurement_data = WorkspaceData.__spectrum_to_measurement_data(
            app_context.current_measured_spectra_for_fit)

        if len(app_context.current_simulations) != 0:
            plot_data = self.__simulation_data(app_context)
            self.main_plot_data.simulation_data = plot_data['simulation']
            self.main_plot_data.residual_data = plot_data['residuals']
            self.simulations = list(app_context.current_simulations.keys())

        return self

    @staticmethod
    def __simulation_data(app_context: AppContext):
        simulation = puke_spectrum(app_context.current_parameters, sims=app_context.current_simulations)
        matched_simulation = match_spectra(simulation, app_context.current_measured_spectra_for_fit)
        residuals = compare_spectra(app_context.current_measured_spectra_for_fit, simulation)
        return {
            'residuals': PlotData(x=matched_simulation[0].x.tolist(), y=residuals.tolist()),
            'simulation': PlotData(x=matched_simulation[0].x.tolist(), y=matched_simulation[0].y.tolist())
        }

    @staticmethod
    def __spectrum_to_measurement_data(spectrum: Spectrum):
        return PlotData(x=spectrum.x.tolist(), y=spectrum.y.tolist())
