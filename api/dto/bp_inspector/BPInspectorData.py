from typing import List

from api.dto.PlotData import PlotData
from api.dto.bp_inspector.ScatterDataSet import ScatterDataSet

ScatterDataSetList = List[ScatterDataSet]


class BPInspectorData:
    def __init__(self):
        self.line_simulation_data: PlotData = PlotData()
        self.simulations_parts: ScatterDataSetList = list()
        self.selected_points_table = list()

    def set_simulation_parts(self, parts: ScatterDataSetList):
        self.simulations_parts = parts

    def set_line_simulation_data(self, x: list, y: list):
        self.line_simulation_data = PlotData(values={'x': list(map(lambda p: float(p), x)), 'y': list(map(lambda p: float(p), y))})
