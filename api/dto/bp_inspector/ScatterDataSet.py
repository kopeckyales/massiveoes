import string

from api.dto.PlotData import PlotData


class ScatterDataSet:
    def __init__(self, key, data: PlotData, simulation: string, vibration_level: int):
        self.scatter_data: PlotData = data
        self.data_key: string = key
        self.simulation: string = simulation
        self.vibration_level: int = vibration_level
