import os

from api import jsonpickle_adapter
from api.AppContext import AppContext, inject_context
import socketio
import eventlet.wsgi

from api.controller import linearization, file_explorer, workspace, bp_tool
from api.exception.AsyncEmitNecessaryException import AsyncEmitNecessaryException

from flask import Flask, Response, send_file

sio = socketio.Server(json=jsonpickle_adapter)
app = Flask(__name__)

# region decorators


def async_emit(func):
    """ Decorates socket message handler with async emit exception handler """
    def func_wrapper(*args):
        try:
            return func(*args)
        except AsyncEmitNecessaryException as e:
            sio.emit(e.emit_name, e.data)
    return func_wrapper


def on(event_name: str):
    """ Groups all used decorators """
    def on_event_decorator(func):
        @sio.on(event_name)
        @inject_context
        @async_emit
        def func_wrapper(*args):
            return func(*args)
        return func_wrapper
    return on_event_decorator
# endregion

# region file explorer actions


@on('file_explorer__list_files')
def file_explorer_list_files(app_context: AppContext, parent_dir: str):
    """" There needs to be unused app_context parameter, because sio injects sid as first parameter """
    return file_explorer.list_files(parent_dir)
# endregion

# region workspace actions


@on('workspace__open_measurement')
def workspace__open_measurement(app_context: AppContext, data):
    return workspace.open_measurement(app_context, data)


@on('workspace__change_set')
def workspace__change_set(app_context: AppContext, data):
    return workspace.change_set(app_context, data)


@on('workspace__list_simulations')
def workspace__list_simulations(*args):
    return workspace.list_simulations()


@on('workspace__add_simulation')
def workspace__list_simulations(app_context: AppContext, data):
    return workspace.add_simulation(app_context, data)


@on('workspace__update_parameters')
def workspace__update_parameters(app_context: AppContext, data):
    return workspace.update_parameters(app_context, data)


@on('workspace__fit')
def workspace__fit(app_context: AppContext, data):
    return workspace.fit(app_context, data)


@on('workspace__copy_parameters')
def workspace__copy_parameters(app_context: AppContext, data):
    return workspace.copy_parameters(app_context, data)


@on('workspace__batch_fit')
def workspace__batch_fit(app_context: AppContext, data):
    for emit_event in workspace.batch_fit(app_context, data):
        # needs to be done in new thread
        sio.emit(emit_event['name'], emit_event['data'])
    return None


@on('workspace__save_project')
def workspace__save_project(app_context: AppContext, data):
    return workspace.save_project(app_context, data)


@on('workspace__export_project')
def workspace__export_project(app_context: AppContext, data):
    return workspace.export_project(app_context, data)
# endregion

# region linearization actions


@on('linearization')
def linearization_default(app_context: AppContext, *args):
    return linearization.default(app_context)


@on('linearization__run')
def linearization_run(app_context: AppContext, data):
    return linearization.run_linearize(app_context, data)


# better approach would be the revert method if something gone wrong
@on('linearization__save')
def linearization_save(app_context: AppContext, *args):
    return linearization.save(app_context)


@on('linearization__rollback')
def linearization__rollback(app_context: AppContext, *args):
    return linearization.rollback(app_context)
# endregion

# region bp_tool


@on('bp_tool__fit')
def bp_tool__fit(app_context: AppContext, data):
    return bp_tool.fit(app_context, data)


@on('bp_tool__points_selected')
def bp_tool__points_selected(app_context: AppContext, data):
    return bp_tool.points_selected(app_context, data)


@on('bp_tool__calc_t_rot')
def bp_tool__calc_t_rot(app_context: AppContext, *args):
    return bp_tool.calc_t_rot(app_context)


@on('bp_tool__calc_t_vib')
def bp_tool__calc_t_vib(app_context: AppContext, *args):
    return bp_tool.calc_t_vib(app_context)


@on('bp_tool__export_to_csv')
def bp_tool__export_to_csv(app_context: AppContext, data):
    return bp_tool.export_to_csv(app_context, data)
# endregion


@sio.on('connect')
def connect(sid, env):
    AppContext.create(sid)


@sio.on('disconnect')
def disconnect(sid):
    AppContext.destroy(sid)


# region flask server
# flask app is here for localhost serving of static files
# do not use flask app on production server, use nginx or another optimized web server
# this methods grants client full server file system access
# !!DO NOT USE THIS METHODS ON REAL WEB SERVER!!
@app.route('/')
def index():
    a = os.path.join(app.wsgi_app.root_path, '../dist/index.html')
    with open(a) as f:
        return f.read()


@app.route('/<path:path>')
def static_content(path):
    mime_types = {
        ".css": "text/css",
        ".js": "application/javascript",
        ".map": "application/javascript",
        ".png": "image/png"
    }
    path = os.path.join(app.wsgi_app.root_path, '../dist', path)
    mime_type = mime_types[os.path.splitext(path)[1]]
    if mime_type == "image/png":
        return send_file(path, mime_type)
    with open(path) as f:
        content = f.read()
    return Response(content, mimetype=mime_type)
# endregion


if __name__ == '__main__':
    app = socketio.Middleware(sio, app)
    eventlet.wsgi.server(eventlet.listen(('localhost', 8888)), app)
